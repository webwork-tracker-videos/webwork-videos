const express = require('express')
const con = require('../../con');
const Deck = require('../../models/deck')
const WebWorkService = require('../../services/WebWorkService');
const webworkService = new WebWorkService;

const router = express.Router()

router.get('/my-team', async (req, res) => {
    try {
        const teamData = await webworkService.getTeamData()
        res.json(teamData)
    } catch(err) {
        res.send(err)
    }
})

router.post('/', async (req, res) => {
    const body = req.body
    try {
        const teamData = await webworkService.getTeamData()

        const decks = await Deck.find().then(async (err, deck) => {
            if (deck) {
                deck.deckData = body.deck
                deck.save()
            } else {
                deck = await Deck.create({
                    teamId: teamData.team_id,
                    deckData: body.deck
                })
            }
            return deck
        })
    } catch (err) {
        res.json(err)
    }
    res.status(201).send()
})

module.exports = router