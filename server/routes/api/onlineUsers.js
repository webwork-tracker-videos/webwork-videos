const express = require('express')
const mongodb = require('mongodb')

const router = express.Router()

// get online users 
router.get('/', async (req, res) => {
    const onlineUsers = await loadOnlineUsersCollection()
    res.send(await onlineUsers.find({}).toArray())
})

// add online users 
router.post('/', async (req, res) => {
    const onlineUsers = await loadOnlineUsersCollection()
    let newObj = await onlineUsers.insertOne({
        userId: req.body.userId,
        status: 0,
        createdAt: new Date()
    })
    res.status(201).send()
})

// add online users 
router.delete('/:id', async (req, res) => {
    const onlineUsers = await loadOnlineUsersCollection()
    await onlineUsers.deleteOne({_id: new mongodb.ObjectID(req.params.id)})
    res.status(200).send()
})

async function loadOnlineUsersCollection () {
    const client = await mongodb.MongoClient.connect
    ('mongodb+srv://admin:fH3cD3h63bfW24k@webwork-videos.kjfdm.mongodb.net/webwork-videos?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })

    return client.db('webwork-videos').collection('onlineUsers')
}

module.exports = router