const express = require('express')
const { validateDeckCreate, validateDeckUpdate } = require('../../middlewares/validators/deckValidator');

var deck_controller = require('../../controllers/api/deckController');

const router = express.Router()

router.get('', deck_controller.list)

router.put('/:id', deck_controller.details)

router.post('/', validateDeckCreate, deck_controller.create)

router.patch('/', validateDeckUpdate, deck_controller.update)

module.exports = router