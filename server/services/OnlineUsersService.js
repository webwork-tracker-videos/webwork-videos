const axios = 'axios'

const url = 'https://localhost:5000/api/online-users'

class OnlineUsersService {

    constructor() {
        this.url = 'https://localhost:5000/api/online-users'
    }
    // get online users
    static getOnlineUsers () {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(this.url)
                const data = res.data
                resolve(data.map(onlineUser => ({
                    ...onlineUser,
                    createdAt: new Date(onlineUser.createdAt)
                })))
            } catch(err) {
                reject(err)
            }
        })
    }

    // insert online users
    static insertOnlineUser (userId) {
        return axios.post(this.url, {
            userId
        })
    }

    // delete online users
    static deleteOnlineUser (id) {
        return axios.delete(`${this.url}${id}`)
    }
}

module.exports = OnlineUsersService