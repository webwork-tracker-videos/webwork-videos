const twilio = require('twilio')
// const axios = require('vue-axios')

class TwilioService {

    constructor() {
        // this.authToken = btoa(process.env.VUE_APP_TWILIO_SID + ':' + process.env.VUE_APP_TWILIO_SECRET)
        // console.log(this);

        // this.axios = axios.create({
        //     baseURL: process.env.VUE_APP_TWILIO_URL
        // });
        
        // this.axios.interceptors.request.use(
        //     function(config) {
        //         const token = localStorage.getItem("token"); 
        //         if (token) {
        //             config.headers["Authorization"] = 'Bearer ' + token;
        //         }
        //         return config;
        //     },
        //     function(error) {
        //         return Promise.reject(error);
        //     }
        // );

        this.twilio = twilio(process.env.VUE_APP_TWILIO_SID, process.env.VUE_APP_TWILIO_SECRET)
    }

    async getRooms(roomName = '', filters = {}) {
        if (typeof filters === 'object' && Object.keys(filters).length) {
            roomName ? filters.uniqueName = roomName : null
            return await this.twilio.video.rooms.list(filters)
        }
        return await this.twilio.video.rooms(roomName)
    }

    async createRoom (roomName, params) {
        params.uniqueName = roomName
        return await this.twilio.video.rooms.create(params)
    }

    async updateRoom (roomName, params) {
        params.uniqueName = roomName
        return await this.twilio.video.rooms.update(params)
    }
}

export default TwilioService