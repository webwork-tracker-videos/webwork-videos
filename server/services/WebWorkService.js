const axios = require('axios')

class WebWorkService {

    constructor() {
        this.url = 'https://www.webwork-tracker.com/chat-api/'
    }
    
    getTeamData () {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(this.url + 'users?user_id=71')
                resolve(res.data)
            } catch(err) {
                reject(err)
            }
        })
    }
    
    // static insertOnlineUser (userId) {
    //     return axios.post(this.url, {
    //         userId
    //     })
    // }
    
    // static deleteOnlineUser (id) {
    //     return axios.delete(`${this.url}${id}`)
    // }
}

module.exports = WebWorkService