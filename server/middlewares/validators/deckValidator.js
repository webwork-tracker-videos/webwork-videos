const {check, validationResult} = require('express-validator');

exports.validateDeckUpdate = [
    check('teamId').exists().isInt(),
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty())
        // return res.status(422).json({errors: errors.array()});
        next();
    }
];

exports.validateDeckCreate = [
    check('deckData').exists().isArray(),
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty())
        return res.status(422).json({errors: errors.array()});
        next();
    },
];