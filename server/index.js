const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

// Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

const decks = require('./routes/api/decks')
const onlineUsers = require('./routes/api/onlineUsers');
const teams = require('./routes/api/teams');

app.use('/api/decks', decks)
app.use('/api/online-users', onlineUsers)
app.use('/api/teams', teams)

// app.use(cors())

const port = process.env.PORT || 5000

app.listen(port, () => console.log(`Server started on port ${port}`))