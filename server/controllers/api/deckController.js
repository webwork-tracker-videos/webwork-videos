const Deck = require('../../models/deck');
const WebWorkService = require('../../services/WebWorkService');

// Display list of all Decks.
exports.list = async (req, res) => {
    try {
        let deckData
        if (req.query.teamId) {
            deckData = await Deck.findOne({
                teamId: req.query.teamId
            })
        } else {
            deckData = await Deck.find()
        }
        res.json(deckData)
    } catch(err) {
        res.send(err)
    }
}

// Display detail page for a specific Deck.
exports.details = async (req, res) => {
    try {
        let deckObj = await Deck.findOne({
            _id: id
        })
        res.json(deckObj)
    } catch(err) {
        res.send(err)
    }
}

// Display Deck create form on GET.
// exports.deck_create_get = function(req, res) {
//     res.send('NOT IMPLEMENTED: Deck create GET');
// };

// Handle Deck create on POST.
exports.create = async (req, res) => {
    const body = req.body
    const service = new WebWorkService;
    const teamData = await service.getTeamData()
    
    try {
        let deck = await Deck.create({
            teamId: teamData.team_id,
            deckData: body.deck
        })
        res.status(201).json(deck)
    } catch (err) {
        res.status(500).json(err)
    }
}

// // Display Deck delete form on GET.
// exports.deck_delete_get = function(req, res) {
//     res.send('NOT IMPLEMENTED: Deck delete GET');
// };

// // Handle Deck delete on POST.
// exports.deck_delete_post = function(req, res) {
//     res.send('NOT IMPLEMENTED: Deck delete POST');
// };

// // Display Deck update form on GET.
// exports.deck_update_get = function(req, res) {
//     res.send('NOT IMPLEMENTED: Deck update GET');
// };

// Handle Deck update on POST.
exports.update = async (req, res) => {
    const { deckData } = req.body
    const service = new WebWorkService;
    const teamData = await service.getTeamData()
    
    try {
        let deck = await Deck.findOneAndUpdate({
            teamId: teamData.team_id,
        }, { deckData }, { new: true })
        res.status(201).send(deck)
    } catch(e) {
        res.send(e)
    }
}