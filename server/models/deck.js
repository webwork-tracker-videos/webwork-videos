let mongoose = require('mongoose')

// Deck schema

const deckSchema = new mongoose.Schema({
    teamId: {
        type: Number,
        required: true,
        unique : true
    },
    deckData: {
        type: Object,
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('Deck', deckSchema)