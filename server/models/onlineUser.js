let mongoose = require('mongoose')

// OnlineUser schema

let onlineUserSchema = mongoose.Schema({
    teamId: {
        type: Number,
        required: true
    },
    userId: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('OnlineUser', onlineUserSchema)