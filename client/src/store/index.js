import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import deck from './modules/deck'
import online_user from './modules/online_user'
import team from './modules/team'

Vue.use(Vuex)

export const store = new Vuex.Store({
    modules: {
        auth,
        deck,
        online_user,
        team
    }
})
