import axios from 'vue-axios'

const state = {
    token: null,
    user: null
}

const getters = {
    loggedIn: state => !!state.user,
    firstName: state => state.user ? state.user.first_name : '',
    currentRole: state => state.user ? state.user.currentRole : { id: null },
    userId: state => state.user ? state.user.id : null,
    user: state => state.user ? state.user : null,
}

const mutations = {
    setToken (state, token) {
        state.token = token
    },
    setUser (state, user) {
        state.user = user
    }
}

const actions = {
    login ({ commit, state, dispatch }, credentials) {
        return new Promise ((resolve, reject) => {
            axios.post('login', {
                email: credentials.email,
                password: credentials.password
            }).then(res => {
                if (res.data.token) {
                    let token = res.data.token
                    commit('setToken', token)
                    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
                    dispatch('getAuthUser').then(() => {
                        dispatch('userSettings/guessTimezone', null, { root: true })
                    })
                    resolve()
                } else {
                    reject(res)
                }
            }).catch(e => { reject(e) })
        })
    },
    // logout ({ commit }) {
    //     return new Promise ((resolve, reject) => {
    //         axios({
    //             url: 'logout',
    //             baseURL: '/',
    //             method: 'POST'
    //         })
    //             .then(res => {
    //                 commit('setToken', null)
    //                 commit('setUser', null)
    //                 commit('userSettings/setLastTimeZoneCheck', null, { root: true })
    //                 resolve()
    //             })
    //             .catch(e => {
    //                 reject(e)
    //             })
    //     })
    // },
    getAuthUser ({ commit, getters }) {
        return new Promise((resolve, reject) => {
            axios.get('user').then(res => {
                commit('setUser', res.data)
                resolve()
            }).catch(e => {
                if (e.response.status === 429)
                    return
                commit('setUser', null)
                commit('userSettings/setLastTimeZoneCheck', null, { root: true })
                commit('setToken', null)
                reject(e)
            })
        })
    },
    // emailLogin ({ commit, dispatch }, token) {
    //     commit('setToken', token)
    //     axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    //     dispatch('getAuthUser').then(() => {
    //         dispatch('userSettings/guessTimezone', null, { root: true })
    //     })
    // },
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
