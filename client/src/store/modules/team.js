import axios from 'vue-axios'
import Vue from 'vue'

const state = {
    teamData: {},
    owner: {},
    users: {},
    baseAvatarURL: 'https://www.webwork-tracker.com/avatars/'
}

const getters = {
    deck: state => Vue._.isEmpty(state.deck) ? null : state.deck,
}

const mutations = {
    SET_DECK (state, deck) {
        state.deck = deck
    }
}

const actions = {
    setDeck ({ commit }) {
        return new Promise ((resolve, reject) => {
            axios.get('/decks?teamId=71').then((res) => {
                let deck = res.data
                commit('SET_DECK', deck)
                resolve(deck)
            }).catch(e => {
                reject(e)
            })
        })
    },
    updateDeck ({ commit }, data) {
        return new Promise ((resolve, reject) => {
            axios.patch('/decks', data).then((res) => {
                let deck = res.data
                commit('SET_DECK', deck)
                resolve(deck)
            }).catch(e => {
                reject(e)
            })
        })
    },
    getDeck ({ commit, getters, dispatch }) {
        return new Promise((resolve, reject) => {
            if (getters.deck) resolve(getters.deck)

            dispatch('setDeck').then(res => {
                resolve(res)
            }).catch(e => {
                reject(e)
            })
        })
    },
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}