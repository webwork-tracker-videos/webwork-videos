import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import cors from 'vue-axios-cors'
import App from './App.vue'
import { store } from './store'
import VueLodash from 'vue-lodash'
import lodash from 'lodash'

Vue.config.productionTip = false

Vue.use(VueLodash, { lodash })
Vue.use(VueAxios, axios)
Vue.use(cors)
Vue.use(store)

Vue.axios.defaults.baseURL = process.env.VUE_APP_BASE_URL

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
